package com.itb.geoyou.viewmodel

import android.app.Activity
import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.itb.geoyou.models.Marker
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import java.util.logging.Handler

class GeoYouViewModel: ViewModel() {
    var markerList = mutableListOf<Marker>()
    var searchList = mutableListOf<Marker>()
    var markerListLD = MutableLiveData<MutableList<Marker>>()
    var coords = LatLng(0.0,0.0)
    var isHolded = false
    var mapThemesList = mutableListOf<String>("Standard","Retro","Night")
    var mapTheme = ""
    var saveImage: Bitmap? = null
    var counter = 0
    var ft: Boolean = true

    fun chargeMarkers(){
        markerList.clear()
        Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser?.email.toString()).collection("markers").get().addOnSuccessListener {
            for(doc in it){
                val marker = Marker(doc.id.toInt(), doc.getString("markername")!!,doc.getString("markertype")!!, doc.getString("markerdesc")!!, LatLng(doc.getGeoPoint("markercoords")!!.latitude, doc.getGeoPoint("markercoords")!!.longitude),doc.getString("markerdate"),null)
                markerList.add(marker)
                markerListLD.postValue(markerList)
                counter = markerList.last().id+1
            }
        }
    }

    fun addMarker(marker: Marker){
        Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser?.email.toString()).collection("markers").document(counter.toString()).set(
            hashMapOf("markername" to marker.markerName,
                "markertype" to marker.markerType,
                "markerdesc" to marker.markerDescription,
                "markercoords" to GeoPoint(coords.latitude, coords.longitude),
                "markerdate" to marker.markerDate))
        val baos = ByteArrayOutputStream()
        marker.markerImg?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()
        val storageRef = FirebaseStorage.getInstance().reference
        val mountainsRef = storageRef.child("images/"+FirebaseAuth.getInstance().currentUser?.email.toString()+"/"+marker.id.toString())
        mountainsRef.putBytes(data)

        markerList.add(marker)
        markerListLD.postValue(markerList)
        counter = markerList.last().id+1
    }

    fun updateMarker(id: Int, marker: Marker){
        Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser?.email.toString()).collection("markers").document(id.toString()
        ).update(
            mapOf("markername" to marker.markerName,
                "markertype" to marker.markerType,
                "markerdesc" to marker.markerDescription))
        val storageRef = FirebaseStorage.getInstance().reference
        val baos = ByteArrayOutputStream()
        marker.markerImg?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()
        val mountainsRef = storageRef.child("images/"+FirebaseAuth.getInstance().currentUser?.email.toString()+"/"+marker.id.toString())
        mountainsRef.putBytes(data)
        markerList.filter { it.id == id }[0].markerName = marker.markerName
        markerList.filter { it.id == id }[0].markerType = marker.markerType
        markerList.filter { it.id == id }[0].markerDescription = marker.markerDescription
        markerList.filter { it.id == id }[0].markerImg = marker.markerImg
        markerListLD.postValue(markerList)
    }

    fun deleteOneMarker(position: Int){
        Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser?.email.toString()).collection("markers").document(markerList[position].id.toString()).delete()
        FirebaseStorage.getInstance().reference.child("images/"+ FirebaseAuth.getInstance().currentUser?.email.toString()+"/"+markerList[position].id).delete()
        markerList.removeAt(position)
        markerListLD.postValue(markerList)
    }

    fun normalList(){
        markerListLD.postValue(markerList)
    }

    fun searchList(string: String){
        searchList.removeAll(searchList)
        searchList = markerList.filter { it.markerName.contains(string, ignoreCase = true) || it.markerType.contains(string, ignoreCase = true)} as MutableList<Marker>
        markerListLD.postValue(searchList)
    }
}