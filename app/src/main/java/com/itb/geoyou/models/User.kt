package com.itb.geoyou.models

data class User(var id: Int, var userName: String, var userPassword: String, var userImage: String)
