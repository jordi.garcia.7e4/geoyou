package com.itb.geoyou.models

import android.graphics.Bitmap
import com.google.android.gms.maps.model.LatLng

data class Marker(var id: Int, var markerName: String, var markerType: String, var markerDescription: String, var markerCoords: LatLng, var markerDate: String?, var markerImg: Bitmap? = null)
