package com.itb.geoyou.adapter

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.itb.geoyou.R
import com.itb.geoyou.fragments.MarkerListFragmentDirections
import com.itb.geoyou.models.Marker
import com.itb.geoyou.viewmodel.GeoYouViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import java.util.logging.Handler

class MarkerListAdapter(private val viewModel: GeoYouViewModel) : RecyclerView.Adapter<MarkerListAdapter.MarkerListViewHolder>(){
    var markers = mutableListOf<Marker>()

    fun setMarkerList(markerList: MutableList<Marker>){
        markers = markerList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarkerListViewHolder {
        // Estem creant la vista del layout del item
        val view = LayoutInflater.from(parent.context).inflate(R.layout.marker,parent,false)

        // I retornem un FiewListViewHolder que gestionara aquesta vista
        return MarkerListViewHolder(view)
    }

    override fun onBindViewHolder(holder: MarkerListViewHolder, position: Int) {
        // Cridarem al holder (FilmListViewHolder) pasanli per parametre un item de la llista
        holder.bindData(markers[position], viewModel)

        holder.itemView.setOnClickListener {
            val action = MarkerListFragmentDirections.listToView(markers[position].id)
            Navigation.findNavController(holder.itemView).navigate(action)
        }
    }

    override fun getItemCount(): Int {
        // Retorno la mida de la llista
        return markers.size
    }

    // Aquesta clase representa una fila concreta del RecycleView i s'encarrega de dibuixar aquesta fila
    inner class MarkerListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var markerName: TextView
        private var markerType: TextView
        private var markerDate: TextView
        private var markerImage: ImageView

        init {
            markerName = itemView.findViewById(R.id.markerName)
            markerType = itemView.findViewById(R.id.markerType)
            markerDate = itemView.findViewById(R.id.markerDate)
            markerImage = itemView.findViewById(R.id.markerImage)
        }

        // Funcio que servira per assignar la informacio d'un item de la llista a la vista que el representa
        fun bindData(marker: Marker, viewModel: GeoYouViewModel) {
            markerName.text = marker.markerName
            markerType.text = marker.markerType
            markerDate.text = marker.markerDate

            if (!viewModel.ft) {
                if (marker.markerImg != null) {
                    markerImage.setImageBitmap(marker.markerImg)
                }
            } else {
                var img: Uri? = null

                val storageRef = FirebaseStorage.getInstance().reference
                storageRef.child("images/" + FirebaseAuth.getInstance().currentUser?.email.toString() + "/" + marker.id).downloadUrl.addOnSuccessListener { ur ->
                    img = ur
                }.addOnFailureListener {
                }

                android.os.Handler(Looper.getMainLooper()).postDelayed({
                    if (img != null) {
                        Glide.with(itemView.context).asBitmap().load(img)
                            .into(object : CustomTarget<Bitmap>() {
                                override fun onResourceReady(
                                    resource: Bitmap,
                                    transition: Transition<in Bitmap>?
                                ) {
                                    markerImage.setImageBitmap(resource)
                                    marker.markerImg = resource
                                }

                                override fun onLoadCleared(placeholder: Drawable?) {
                                }

                            })
                    }
                }, 1000)
            }
        }
    }
}