package com.itb.geoyou.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.itb.geoyou.R
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class RegisterFragment : Fragment(R.layout.fragment_register) {
    lateinit var goToLogin: ImageView
    lateinit var email: TextInputLayout
    lateinit var password: TextInputLayout
    lateinit var repeatPassword: TextInputLayout
    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        goToLogin = view.findViewById(R.id.goToLogin)
        email = view.findViewById(R.id.emailInputLayout)
        password = view.findViewById(R.id.nameInputLayout)
        repeatPassword = view.findViewById(R.id.repeatPasswordInputLayout)

        goToLogin.setOnClickListener {
            if (!email.editText?.text.isNullOrEmpty() && !password.editText?.text.isNullOrEmpty() && password.editText?.text.toString() == repeatPassword.editText?.text.toString()) {
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                    email.editText?.text.toString(),
                    password.editText?.text.toString()
                )
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            findNavController().navigate(R.id.registerToLog)
                            Firebase.firestore.collection("users").document(email.editText?.text.toString()).set(
                                hashMapOf("username" to "",
                                "img" to "",
                                "theme" to "Retro")
                            )
                        } else {
                            Toast.makeText(
                                view.context,
                                "Error while registering the user, try again",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
            }else if(email.editText?.text.isNullOrEmpty()){
                email.error = "Email is required!"
                Handler(Looper.getMainLooper()).postDelayed({
                    email.isErrorEnabled = false
                }, 4000)
            }else if(password.editText?.text.isNullOrEmpty()){
                password.error = "Password is required!"
                Handler(Looper.getMainLooper()).postDelayed({
                    password.isErrorEnabled = false
                }, 4000)
            }
            else if(repeatPassword.editText?.text.isNullOrEmpty()){
                repeatPassword.error = "Repeating password is required!"
                Handler(Looper.getMainLooper()).postDelayed({
                    repeatPassword.isErrorEnabled = false
                }, 4000)
            }else if(password.editText?.text.toString() != repeatPassword.editText?.text.toString()){
                repeatPassword.error = "Passwords do not match!"
                Handler(Looper.getMainLooper()).postDelayed({
                    repeatPassword.isErrorEnabled = false
                }, 4000)
            }
        }
    }
}