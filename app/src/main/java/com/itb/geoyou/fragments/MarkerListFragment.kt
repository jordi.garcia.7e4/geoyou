package com.itb.geoyou.fragments

import android.graphics.Canvas
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.itb.geoyou.R
import com.itb.geoyou.adapter.MarkerListAdapter
import com.itb.geoyou.viewmodel.GeoYouViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator

class MarkerListFragment : Fragment(R.layout.fragment_marker_list) {
    private  lateinit var recyclerView: RecyclerView
    private val viewModel: GeoYouViewModel by activityViewModels()
    lateinit var searchInputLayout: TextInputLayout

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        val principalAdapter = MarkerListAdapter(viewModel)

        recyclerView = view.findViewById(R.id.recycler_view)
        searchInputLayout = view.findViewById(R.id.searchInputLayout)
        recyclerView.layoutManager = LinearLayoutManager(view.context)

        startFunc()

        searchInputLayout.setEndIconOnClickListener {
            viewModel.searchList(searchInputLayout.editText?.text.toString())
        }

        ItemTouchHelper(object  : ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT){
            val deleteIcon = R.drawable.trash

            override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
                val swipeFlag = ItemTouchHelper.LEFT
                return ItemTouchHelper.Callback.makeMovementFlags(0, swipeFlag)
            }

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val pos = viewHolder.adapterPosition
                val dialogBuilder = MaterialAlertDialogBuilder(view.context,
                    R.style.MaterialAlertDialog_rounded
                ).setMessage("¡WATCH OUT! ¿ARE YOU SURE YOU WANT TO DELETE THIS ELEMENT? IF SO PRESS DELETE")
                    .setTitle("WARNING")
                    .setCancelable(false)
                    .setNegativeButton("CANCEL") {_,_ ->
                        principalAdapter.notifyItemChanged(pos)
                    }
                    .setPositiveButton("DELETE") { _, _ ->
                        viewModel.deleteOneMarker(pos)
                        principalAdapter.notifyItemRemoved(pos)
                    }.create()
                dialogBuilder.window?.attributes?.windowAnimations = R.style.DialogAnimation
                dialogBuilder.show()
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {

                val deleteColor = ContextCompat.getDrawable(requireContext(), R.drawable.marker_layout)

                deleteColor?.bounds = Rect(
                    viewHolder.itemView.right + dX.toInt() - 100, viewHolder.itemView.top,viewHolder.itemView.right,viewHolder.itemView.bottom
                )

                deleteColor?.draw(c)

                RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    .addSwipeLeftActionIcon(deleteIcon)
                    .create()
                    .decorate()

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }).attachToRecyclerView(recyclerView)


    }

    private fun startFunc(){
        val principalAdapter = MarkerListAdapter(viewModel)
        viewModel.normalList()
        viewModel.markerListLD.observe(viewLifecycleOwner,{principalAdapter.setMarkerList(it) })
        recyclerView.adapter = principalAdapter
    }

    override fun onStop() {
        super.onStop()
        viewModel.ft = false
    }
}