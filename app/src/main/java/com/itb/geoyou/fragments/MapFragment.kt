package com.itb.geoyou.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.itb.geoyou.R
import com.itb.geoyou.viewmodel.GeoYouViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


const val REQUEST_CODE_LOCATION = 100
@SuppressLint("MissingPermission")
class MapFragment : Fragment(R.layout.fragment_map), OnMapReadyCallback, GoogleMap.InfoWindowAdapter {
    lateinit var map: GoogleMap
    lateinit var themeButton: FloatingActionButton
    lateinit var ubiButton: FloatingActionButton
    private val viewModel: GeoYouViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        themeButton = view.findViewById(R.id.themeButton)
        ubiButton = view.findViewById(R.id.ubiButton)

        Handler(Looper.getMainLooper()).postDelayed({
            themeButton.setOnClickListener {
                changeTheme()
            }
            ubiButton.setOnClickListener {
                if(map.isMyLocationEnabled && map.myLocation != null) {
                    myPosAnim()
                }else{
                    enableLocation()
                    Toast.makeText(requireContext(), "We need to use ubication to get your position, please accept the permissions and activate it", Toast.LENGTH_SHORT).show()
                }
            }
            map.setOnMyLocationClickListener {
                saveMyLocation()
                findNavController().navigate(R.id.mapToMarker)
            }
            map.setOnMapLongClickListener {
                viewModel.isHolded = true
                saveLocation(it)
                findNavController().navigate(R.id.mapToMarker)
            }
            map.setOnInfoWindowClickListener {
                val action = MapFragmentDirections.mapToMarker(it.tag as Int)
                findNavController().navigate(action)
            }
        }, 1000)
    }

    fun saveMyLocation(){
        viewModel.coords = LatLng(map.myLocation.latitude, map.myLocation.longitude)
    }

    fun saveLocation(coords: LatLng){
        viewModel.coords = LatLng(coords.latitude, coords.longitude)
    }

    fun createMap(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        enableLocation()
        viewModel.markerListLD.observe(viewLifecycleOwner, {createMarker(it)})

        map.setInfoWindowAdapter(this)
        map.uiSettings.isMyLocationButtonEnabled = false

        if (viewModel.mapTheme == "") {
            Firebase.firestore.collection("users")
                .document(FirebaseAuth.getInstance().currentUser?.email.toString()).get()
                .addOnSuccessListener {
                    viewModel.mapTheme = it.getString("theme")!!
                }
            Handler(Looper.getMainLooper()).postDelayed({
                themeChanger()
            }, 1000)
        }else {
            themeChanger()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView =  inflater.inflate(R.layout.fragment_map, container, false)
        createMap()
        return rootView
    }

    fun createMarker(markerList: MutableList<com.itb.geoyou.models.Marker>){
        for(marker in markerList){
            val coordinates = LatLng(marker.markerCoords.latitude,marker.markerCoords.longitude)
            val myMarker = MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.geoyoupin2)).position(coordinates).title(marker.markerName)
            val add = map.addMarker(myMarker)
            add!!.tag = marker.id
        }
    }

    fun myPosAnim(){
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    map.myLocation.latitude,
                    map.myLocation.longitude
                ), 18f
            ),
            5000, null
        )
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "We need to use ubication to get your position, please accept the permissions and activate it", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "We need to use ubication to get your position, please accept the permissions and activate it",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun changeTheme(){
        val builder = layoutInflater.inflate(R.layout.theme_dialog, null)
        val themeInput: AutoCompleteTextView = builder.findViewById(R.id.themeInput)
        themeInput.setAdapter(ArrayAdapter(requireContext(), R.layout.support_simple_spinner_dropdown_item, viewModel.mapThemesList))
        val dialogBuilder = MaterialAlertDialogBuilder(view?.context!!,
            R.style.MaterialAlertDialog_rounded
        ).setView(builder)
            .setCancelable(false).setNegativeButton("CANCEL"){_,_ ->}.setPositiveButton("APPLY"){ _, _ ->
                viewModel.mapTheme = themeInput.text.toString()
                themeChanger()
                Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser?.email.toString()).update(
                    mapOf("theme" to themeInput.text.toString()))
            }.create()
        dialogBuilder.window?.attributes?.windowAnimations = R.style.DialogAnimation
        dialogBuilder.show()

    }

    fun themeChanger(){
        when(viewModel.mapTheme){
            "Retro" -> map.setMapStyle(MapStyleOptions.loadRawResourceStyle(view?.context!!, R.raw.restro_style))
            "Standard" -> map.setMapStyle(MapStyleOptions.loadRawResourceStyle(view?.context!!, R.raw.standard_style))
            "Night" -> map.setMapStyle(MapStyleOptions.loadRawResourceStyle(view?.context!!, R.raw.night_style))
        }
    }

    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "We need to use ubication to get your position, please accept the permissions and activate it",
                Toast.LENGTH_SHORT).show()
        }
    }

    override fun onStop() {
        super.onStop()

        if (map.isMyLocationEnabled) {
            if (map.myLocation != null && !viewModel.isHolded) {
                saveMyLocation()
            } else {
                viewModel.isHolded = false
            }
        }
    }

    override fun getInfoContents(p0: Marker): View? {
        val view = layoutInflater.inflate(R.layout.little_marker, null)

        setWindowData(view,p0)

        return view
    }

    override fun getInfoWindow(p0: Marker): View? {
        val view = layoutInflater.inflate(R.layout.little_marker, null)

        setWindowData(view,p0)

        return view
    }

    fun setWindowData(view: View, marker: Marker){
        val markerName: TextView = view.findViewById(R.id.markerName)
        val markerType: TextView = view.findViewById(R.id.markerType)

        val markerSelected = viewModel.markerList.filter { it.id == marker.tag }[0]

        markerName.text = markerSelected.markerName
        markerType.text = markerSelected.markerType
    }
}