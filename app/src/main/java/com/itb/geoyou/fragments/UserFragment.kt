package com.itb.geoyou.fragments

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.ImageView
import androidx.navigation.fragment.findNavController
import com.itb.geoyou.R
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso

class UserFragment : Fragment(R.layout.fragment_user) {
    lateinit var goToLogin: ImageView
    lateinit var saveUser: ImageView
    lateinit var userImage: ImageView
    lateinit var emailInputLayout: TextInputLayout
    lateinit var nameInputLayout: TextInputLayout
    lateinit var photoInputLayout: TextInputLayout

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        goToLogin = view.findViewById(R.id.logOutButton)
        saveUser = view.findViewById(R.id.saveButton)
        userImage = view.findViewById(R.id.userImage)
        emailInputLayout = view.findViewById(R.id.emailInputLayout)
        nameInputLayout = view.findViewById(R.id.nameInputLayout)
        photoInputLayout = view.findViewById(R.id.photoInputLayout)

        loadData()

        goToLogin.setOnClickListener {
            FirebaseAuth.getInstance().signOut()

            findNavController().navigate(R.id.userToLog)
        }

        saveUser.setOnClickListener {
            //FirebaseAuth.getInstance().currentUser?.updateEmail(emailInputLayout.editText?.text.toString())
            FirebaseAuth.getInstance().currentUser?.updateProfile(UserProfileChangeRequest.Builder().setDisplayName(nameInputLayout.editText?.text.toString()).build())
            FirebaseAuth.getInstance().currentUser?.updateProfile(UserProfileChangeRequest.Builder().setPhotoUri(Uri.parse(photoInputLayout.editText?.text.toString())).build())

            Firebase.firestore.collection("users").document(FirebaseAuth.getInstance().currentUser?.email.toString()).update(
                mapOf("username" to nameInputLayout.editText?.text.toString(),
                "img" to photoInputLayout.editText?.text.toString())
            )
        }
    }

    private fun loadData() {
        val user = FirebaseAuth.getInstance().currentUser

        emailInputLayout.editText?.setText(user?.email)
        nameInputLayout.editText?.setText(user?.displayName)
        photoInputLayout.editText?.setText(user?.photoUrl.toString())
        Picasso.get().load(user?.photoUrl).error(R.drawable.noimage).into(userImage)
    }
}