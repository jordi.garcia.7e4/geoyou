package com.itb.geoyou.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.itb.geoyou.R
import com.itb.geoyou.viewmodel.GeoYouViewModel
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth

class LoginFragment : Fragment(R.layout.fragment_login) {
    lateinit var goToRegister: TextView
    lateinit var loginButton: ImageView
    lateinit var email: TextInputLayout
    lateinit var password: TextInputLayout
    private val viewModel: GeoYouViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        goToRegister = view.findViewById(R.id.goToRegister)
        loginButton = view.findViewById(R.id.loginButton)
        email = view.findViewById(R.id.emailInputLayout)
        password = view.findViewById(R.id.nameInputLayout)
        viewModel.ft = true

        if (FirebaseAuth.getInstance().currentUser != null){
            findNavController().navigate(R.id.mapFragment)
            viewModel.chargeMarkers()
        }

        goToRegister.setOnClickListener {
            findNavController().navigate(R.id.logToRegister)
        }

        loginButton.setOnClickListener {
            if (!email.editText?.text.isNullOrEmpty() && !password.editText?.text.isNullOrEmpty()) {
                FirebaseAuth.getInstance().signInWithEmailAndPassword(
                    email.editText?.text.toString(),
                    password.editText?.text.toString()
                )
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            findNavController().navigate(R.id.logToMap)
                            viewModel.chargeMarkers()
                        } else {
                            Toast.makeText(view.context, "Error while login, check if the email and password match", Toast.LENGTH_LONG)
                                .show()
                        }
                    }
            }else if(email.editText?.text.isNullOrEmpty()){
                email.error = "Email is required!"
                Handler(Looper.getMainLooper()).postDelayed({
                    email.isErrorEnabled = false
                }, 4000)
            }else if(password.editText?.text.isNullOrEmpty()){
                password.error = "Password is required!"
                Handler(Looper.getMainLooper()).postDelayed({
                    password.isErrorEnabled = false
                }, 4000)
            }
        }
    }
}