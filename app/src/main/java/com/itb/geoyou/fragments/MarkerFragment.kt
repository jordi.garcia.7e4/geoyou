package com.itb.geoyou.fragments

import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.itb.geoyou.R
import com.itb.geoyou.models.Marker
import com.itb.geoyou.viewmodel.GeoYouViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputLayout
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.drawToBitmap
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import java.text.SimpleDateFormat
import java.util.*

class MarkerFragment : Fragment(R.layout.fragment_marker) {
    lateinit var updateButton: ImageView
    lateinit var addButton: ImageView
    lateinit var markerImage: ImageView
    lateinit var nameInputLayout: TextInputLayout
    lateinit var typeInputLayout: TextInputLayout
    lateinit var coordsInputLayout: TextInputLayout
    lateinit var descrInputLayout: TextInputLayout
    lateinit var imageButton: FloatingActionButton
    lateinit var deleteImageButton: FloatingActionButton
    lateinit var galleryImageButton: FloatingActionButton
    private val viewModel: GeoYouViewModel by activityViewModels()
    var bitmap: Bitmap? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        updateButton = view.findViewById(R.id.updateButton)
        addButton = view.findViewById(R.id.addButton)
        markerImage = view.findViewById(R.id.markerImage)
        nameInputLayout = view.findViewById(R.id.nameInputLayout)
        typeInputLayout = view.findViewById(R.id.typeInputLayout)
        coordsInputLayout = view.findViewById(R.id.coordsInputLayout)
        descrInputLayout = view.findViewById(R.id.descrInputLayout)
        imageButton = view.findViewById(R.id.imageButton)
        deleteImageButton = view.findViewById(R.id.deleteImageButton)
        galleryImageButton = view.findViewById(R.id.galleryImageButton)

        loadData()

        updateButton.setOnClickListener {
            updateMarker()
        }

        addButton.setOnClickListener {
            addMarker()
        }

        imageButton.setOnClickListener{
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            try{
                startActivityForResult(intent,1)
            }catch (e: ActivityNotFoundException){
                Toast.makeText(
                    view.context,
                    "Error accessing to the camera",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        deleteImageButton.setOnClickListener {
            markerImage.setImageBitmap(null)
            bitmap = null
            viewModel.saveImage = null
        }

        galleryImageButton.setOnClickListener {
            val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            try{
                startActivityForResult(pickPhoto,2)
            }catch (e: ActivityNotFoundException){
                Toast.makeText(
                    view.context,
                    "Error accessing to the camera roll",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun loadData() {
        if (!arguments?.isEmpty!!){
            val id = arguments?.getInt("id")
            val marker = viewModel.markerList.filter { it.id == id }[0]

            nameInputLayout.editText?.setText(marker.markerName)
            typeInputLayout.editText?.setText(marker.markerType)
            coordsInputLayout.editText?.setText("${marker.markerCoords.latitude}, ${marker.markerCoords.longitude}")
            descrInputLayout.editText?.setText(marker.markerDescription)

            if (marker.markerImg != null) {
                markerImage.setImageBitmap(marker.markerImg)
            } else {
                var img: Uri? = null

                val storageRef = FirebaseStorage.getInstance().reference
                storageRef.child("images/"+ FirebaseAuth.getInstance().currentUser?.email.toString()+"/"+marker.id).downloadUrl.addOnSuccessListener { ur ->
                    img = ur
                }.addOnFailureListener {
                }

                Handler(Looper.getMainLooper()).postDelayed({
                    if (img != null){
                        Glide.with(view?.context!!).asBitmap().load(img).into(object : CustomTarget<Bitmap>(){
                            override fun onResourceReady(
                                resource: Bitmap,
                                transition: Transition<in Bitmap>?
                            ) {
                                markerImage.setImageBitmap(resource)
                                marker.markerImg = resource
                            }
                            override fun onLoadCleared(placeholder: Drawable?) {
                            }

                        })
                    }
                },1000)
            }

        }else{
            coordsInputLayout.editText?.setText("${viewModel.coords.latitude}, ${viewModel.coords.longitude}")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == RESULT_OK){
            val imageBitmap = data!!.extras!!.get("data")
            bitmap = imageBitmap as Bitmap
            markerImage.setImageBitmap(bitmap)
        }else if (requestCode == 2 && resultCode == RESULT_OK){
            val selectedImage: Uri = data?.data!!
            bitmap = MediaStore.Images.Media.getBitmap(requireContext().contentResolver,selectedImage);
            markerImage.setImageURI(selectedImage)
        }
    }

    fun updateMarker(){
        if (!arguments?.isEmpty!!){
            val id = arguments?.getInt("id")
            if (nameInputLayout.editText?.text.isNullOrEmpty()) {
                nameInputLayout.error = "Name is required!"
                Handler(Looper.getMainLooper()).postDelayed({
                    nameInputLayout.isErrorEnabled = false
                }, 4000)
            }else if(typeInputLayout.editText?.text.isNullOrEmpty()){
                typeInputLayout.error = "Type is required!"
                Handler(Looper.getMainLooper()).postDelayed({
                    typeInputLayout.isErrorEnabled = false
                }, 4000)
            }else {
                viewModel.updateMarker(
                    id!!,
                    Marker(
                        id,
                        nameInputLayout.editText?.text.toString(),
                        typeInputLayout.editText?.text.toString(),
                        descrInputLayout.editText?.text.toString(),
                        viewModel.coords, null,
                        markerImage.drawable.toBitmap()
                    )
                )
                findNavController().navigate(R.id.markerToMap)
            }
        }else{
            Toast.makeText(
                view?.context,
                "This Marker can't be updated because is nonexistent, try creating it!",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    fun addMarker(){
        if (!nameInputLayout.editText?.text.isNullOrEmpty() && !typeInputLayout.editText?.text.isNullOrEmpty()) {
            val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
            val currentDate = sdf.format(Date())

            viewModel.addMarker(
                Marker(
                    viewModel.counter,
                    nameInputLayout.editText?.text.toString(),
                    typeInputLayout.editText?.text.toString(),
                    descrInputLayout.editText?.text.toString(),
                    viewModel.coords, currentDate,
                    bitmap
                )
            )
            findNavController().navigate(R.id.markerToMap)
        }else if (nameInputLayout.editText?.text.isNullOrEmpty()) {
            nameInputLayout.error = "Name is required!"
            Handler(Looper.getMainLooper()).postDelayed({
                nameInputLayout.isErrorEnabled = false
            }, 4000)
        }else if(typeInputLayout.editText?.text.isNullOrEmpty()){
            typeInputLayout.error = "Type is required!"
            Handler(Looper.getMainLooper()).postDelayed({
                typeInputLayout.isErrorEnabled = false
            }, 4000)
        }
    }

//    override fun onPause() {
//        super.onPause()
//
//        if (bitmap != null) {
//            viewModel.saveImage = bitmap
//        }
//    }
//
//    override fun onResume() {
//        super.onResume()
//
//        if (viewModel.saveImage != null) {
//            bitmap = viewModel.saveImage
//            markerImage.setImageBitmap(bitmap)
//        }
//    }
}